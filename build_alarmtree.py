import sys
import os
import configparser


os.system("rm CHOP_CHIC.def")
os.system("rm CHOP_DRV.def")

os.system("wget https://bitbucket.org/europeanspallationsource/chop_chic/raw/HEAD/CHOP_CHIC.def")
os.system("wget https://bitbucket.org/europeanspallationsource/chop_drv/raw/HEAD/CHOP_DRV.def")



config = configparser.ConfigParser()
config.read("alarmtree.ini")


instruments = config.sections()

instruments = instruments[:-1]


f11 = open("CHOP_CHIC.def",'r')
f12 = open("CHOP_DRV.def",'r')

CHIC_pvs = []
Drive_pvs = []


for x in f11:
	if "major" in x or "minor" in x:
		pv = x.split('"')
		if len(pv) > 3:
			CHIC_pvs.append([pv[1],pv[3], "CHIC"])

for y in f12:
	if "major" in y or "minor" in y:
		pv = y.split('"')
		if len(pv) > 3:
			Drive_pvs.append([pv[1], pv[3], "Drive"])


pvs = []
desc= []


for i in instruments:
	drives = config.get(i,"DRIVES").split(',')
	pvs.append(["INSTRUMENT_START",i, " ", " "])
	for z in range(0,int(config.get(i,"CHIC"))):
		pvs.append(["CHIC_START","CHIC0"+str(z+1), " ", " "])
		for x in CHIC_pvs:
			pvs.append([config.get(i, "DIV")+"-"+config.get(i, "NAME")+":"+config.get("DEVICE","CHIC")+str(z+1)+":"+x[0],x[1], "CHIC0"+str(z+1),config.get(i,"NAME")])
		for k in range(0,int(drives[z])):
			pvs.append(["DRIVE_START","Drive"+str(k+1), " ", " "])
			for y in Drive_pvs:
				pvs.append([config.get(i, "DIV")+"-"+config.get(i, "NAME")+":"+config.get("DEVICE","DRIVE")+"-0"+str(z+1)+"0"+str(k+1)+":"+y[0], y[1], "Drive"+str(k+1),config.get(i,"NAME")])
			pvs.append(["DRIVE_END"," ", " ", " "]) 
		pvs.append(["CHIC_END"," ", " ", " "])
	pvs.append(["INSTRUMENT_END"," ", " ", " "])

f = open("Chopper_CHIC.xml", 'w')

f3 = open(".ignore_list.txt", 'r')

ignoreList = []

for y in f3:
	pv = y.split(':')[-1]
	ignoreList.append(pv)


f.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n")
f.write("<config name=\"Chopper_CHIC\">\n")

for i in range(0,len(pvs)):
	ignore = 0
	for j in ignoreList:
		if j == i.split(':')[-1]:
			ignore = 1
	if ignore == 0:
		if "INSTRUMENT_START" in pvs[i][0]:
			f.write("  <component name=\""+pvs[i][1]+"\">\n")
		if "INSTRUMENT_END" in pvs[i][0]:
			f.write("  </component>\n")
		if "CHIC_START" in pvs[i][0]:
			f.write("      <component name=\""+pvs[i][1]+"\">\n")
		if "CHIC_END" in pvs[i][0]:
			f.write("      </component>\n")
		if "DRIVE_START" in pvs[i][0]:
			f.write("        <component name=\""+pvs[i][1]+"\">\n") 
		if "DRIVE_END" in pvs[i][0]:
			f.write("        </component>\n")
		if "CHIC" in pvs[i][2]:
			f.write("          <display>\n")
			f.write("          <title>Alarm Details</title>\n")
			f.write("          <details>test_alarm2.bob</details>\n")
			f.write("          </display>\n")
			f.write("          <pv name=\""+pvs[i][0]+"\">\n")
			f.write("            <description>"+pvs[i][1]+"</description>\n")
			f.write("            <latching>false</latching>\n")
			f.write("            <annunciating>false</annunciating>\n")
			f.write("          </pv>\n")
		if "Drive" in pvs[i][2]:
			f.write("            <display>\n")
			f.write("            <title>Alarm Details</title>\n")
			f.write("            <details>test_alarm2.bob</details>\n")
			f.write("            </display>\n")
			f.write("            <pv name=\""+pvs[i][0]+"\">\n")
			f.write("              <description>"+pvs[i][1]+"</description>\n")
			f.write("              <latching>false</latching>\n")
			f.write("              <annunciating>false</annunciating>\n")
			f.write("            </pv>\n")
f.write("</config>\n")
